<?php

/**
 * Implementation of hook_block
 */
function latest_members_block($op = 'list', $delta = 0, $edit = array()) {

  switch ($op) {
  
  case 'list':
    $blocks[0]['info'] = t('Latest Members');
    return $blocks;
  
  case 'configure':
    $form['latest_members_block_num_members'] = array (
      '#type' => 'textfield', 
      '#title' => t('Number of latest members to show in block'),
      '#default_value' => variable_get('latest_members_block_num_members', 6),
    ); 
    $form['latest_members_block_cache_time'] = array (
      '#type' => 'textfield', 
      '#title' => t('Cache time'),
      '#default_value' => variable_get('latest_members_block_cache_time', 3600),
      '#description' => t('Time to cache the latest members block in seconds'),
    ); 
    return $form;
  	
  case 'save':
    variable_set('latest_members_block_num_members', (int) $edit['latest_members_block_num_members']);
    variable_set('latest_members_block_cache_time', (int) $edit['latest_members_block_cache_time']);
    break;
  
  case 'view':
    $block = array();
    $block['subject'] = t('Latest Members');
    $block['content'] = theme_latest_members(latest_members_list());
    return $block;	
  
  }//end switch
}

/**
 * Get the list of latest active members
 * which will be stored in the cache table
 * @return
 *   array of user pictures  
 */ 
function latest_members_list() {

  // retrieve the list from the cache
  $cache = cache_get('latest_members_list', 'cache');

  if ($cache->expire > time()) {
    $accounts = unserialize($cache->data);
  }
  else {
    $num_members = variable_get('latest_members_block_num_members', 6);
    $query = "SELECT uid, name, picture FROM {users} WHERE status != 0 and uid != 0 AND picture <> '' ORDER BY uid DESC";
    $result = db_query_range($query, NULL, 0, $num_members);
  
    $accounts = array();
    while ($account = db_fetch_object($result)) {
      $accounts[] = theme('user_picture', $account);
    }
	cache_set('latest_members_list', serialize($accounts), $table = 'cache', $expire = time() + variable_get('latest_members_block_cache_time', 3600), $headers = NULL);
  }
	
  return $accounts;
}

/**
 * Generates a display of all users pics if user has a pic and links to their profile page
 * @param $accounts
 *   array of user pictures
 * @return
 *   html output   
 */
function theme_latest_members($accounts) {
  $output = '<div class="latest-members">' . implode(' ', $accounts) . '</div>'; 
  return $output;
}
