General notes
=====================================================

The Latest Members module creates a block that you can use to show the latest members pictures (user pictures) on your site. You can also configure the block to show the number of pictures that you prefer. The default number of new user pictures is 6.

The Latest Members module does something similar as the Avatar Gallery module but with a different approach: it doesn't make use of an image map, simply a themeable set of user pictures.

Initial development was done by dangibas - www.hygen.net and completely rewritten by toemaz according to the Drupal coding standards.
